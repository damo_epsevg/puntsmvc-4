package edu.upc.damo.punts_mvc;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;

import java.util.Random;

public class MainActivity extends Activity {

    static final String TAG ="PROVA";
    static final int DIAMETRE =25;

    private EditText t1;
    private EditText t2;

    private CjtDePunts cjtDePunts = new CjtDePunts();       // El Model
    private VistaDePunts vista;                          // La vista

    private VistaPunt vistaPunt; // Vista d'un punt

    private Random generador = new Random();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        inicialitza();

         vista.defineixModel(cjtDePunts);
         vistaPunt =new VistaPunt(t1,t2);
    }


    /* Inicialització de l'activity */

    private void inicialitza() {
        t1 = (EditText) findViewById(R.id.text1);
        t2 = (EditText) findViewById(R.id.text2);
        vista = (VistaDePunts) findViewById(R.id.vistaDePunts);

        final GeneradorPunt g = new GeneradorPunt();

        findViewById(R.id.botoVerd).setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        nouPunt(g.obtenirGeneradorPunt(null), null, R.color.colorBotoVerd);
                    }
                }
        );

        findViewById(R.id.botoVermell).setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        nouPunt(g.obtenirGeneradorPunt(null), null, R.color.colorBotoVermell);
                    }
                }
        );

        vista.setOnTouchListener(new View.OnTouchListener() {
                                     @Override
                                     public boolean onTouch(View v, MotionEvent event) {
                                         if (MotionEvent.ACTION_DOWN != event.getAction())
                                             return false;

                                         nouPunt(g.obtenirGeneradorPunt(event), event, R.color.colorTouch);
                                         return true;
                                     }
                                 }
        );
    }

    private void nouPunt(GeneradorPunt g, MotionEvent event, int color) {
        Punt p = g.nouPunt(event, getResources().getColor(color));
        cjtDePunts.afegeixPunt(p);  // Afegim al model
        vistaPunt.avisaVistaPunt(p);  // Avisem a la vista
    }



        @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    private void testIteracio(){
        CjtDePunts c = new CjtDePunts();

        c.afegeixPunt(new Punt(10,10, Color.BLUE,2));
        c.afegeixPunt(new Punt(1,1, Color.BLUE,2));
        c.afegeixPunt(new Punt(13,31, Color.BLUE,1));


        for(Punt p: c) {
            Log.i(TAG, p.toString());
        }
    }



      /* ---------------------    Class privade per a la generració dels punts -------------------*/

    private  class GeneradorPunt{

        GeneradorPunt obtenirGeneradorPunt(MotionEvent event){
            if (event==null)
                return new GeneradorPuntAleatori();
            else
                return new GeneradorPuntCoordenades();
        }

        protected Punt obtePunt(MotionEvent event, int c){ return null;}

        public  Punt  nouPunt(MotionEvent event, int c){
            Punt punt = obtePunt(event,c);
            return punt;
        };

        private class GeneradorPuntAleatori extends GeneradorPunt {


            @Override
            protected Punt obtePunt(MotionEvent event, int c) {
                /**
                 * Genera un punt aleatori. El random retorna un valor entre 0 i 1, que considerem com una
                 * fracció de l'espai disponible. Per això multipliquem el valor aleatori per la dimensió.
                 * Per tal d'assegutar que el punt generat cap completament dins dels límits, hi sumem el diàmetre
                 */

                return new Punt(generador.nextFloat()*vista.getWidth()+DIAMETRE,
                        generador.nextFloat()*vista.getHeight()+DIAMETRE,
                        c, DIAMETRE);
            }
        }

        private class GeneradorPuntCoordenades extends GeneradorPunt {

            @Override
            protected Punt obtePunt(MotionEvent event, int c){
                return new Punt(event.getX(), event.getY(),c, DIAMETRE);
            }
        }
    }


     /* --------------------------------------------------------------------------------------*/


}
